﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class YearProductionInformation
    {
        public int YearID;
        public int? Year { get; set; }
    }
}
