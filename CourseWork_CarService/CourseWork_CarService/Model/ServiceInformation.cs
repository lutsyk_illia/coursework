﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class ServiceInformation
    {
        public int Serviceid;
        public string ServiceName { get; set; }
        public decimal? ServicePrice { get; set; }
        public int? ExecutionTime { get; set; }
    }
}
