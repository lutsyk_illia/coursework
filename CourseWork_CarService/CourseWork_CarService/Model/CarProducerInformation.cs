﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class CarProducerInformation
    {
        public int CarProducerId;
        public string CarProducer_Name { get; set; }
    }
}
