﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class CarInformation
    {
        public int Car_id;
        public string Producer { get; set; }
        public string Brand { get; set; }
        public int? Year { get; set; }
        public string Color { get; set; }
        public string EngineNumber { get; set; }
        public string CarNumber { get; set; }
        public DateTime? DateOfRegistration { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerFirstName { get; set; }
    }
}
