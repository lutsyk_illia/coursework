﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class ClientInformation
    {
        public int Clientid;
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Homeaddress { get; set; }
        public string Phone { get; set; }
    }
}
