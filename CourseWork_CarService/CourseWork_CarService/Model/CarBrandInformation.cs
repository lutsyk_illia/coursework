﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class CarBrandInformation
    {
        public int CarBrandId;
        public string CarProducer_Name { get; set; }
        public string Brand { get; set; }
    }
}
