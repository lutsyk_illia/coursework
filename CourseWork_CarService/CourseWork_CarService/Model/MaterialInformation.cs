﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class MaterialInformation
    {
        public int MaterialId;
        public string MaterialName { get; set; }
        public DateTime? DateOfIssue { get; set; }
        public decimal? Price { get; set; }
    }
}
