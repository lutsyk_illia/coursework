﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class PositionInformation
    {
        public int PositionId;
        public string PositionName { get; set; }
    }
}
