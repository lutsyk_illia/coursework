﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class WorkerInformation
    {
        public int Workerid;
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string PositionName { get; set; }
    }
}
