﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class OrderInformation
    {
        public int Order_Number { get; set; }
        public string Car_Number { get; set; }
        public string Client_Firstname { get; set; }
        public string Client_Lastname { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public decimal? Order_Discount { get; set; }
        public decimal? Final_Price { get; set; }
        public DateTime? Date_Of_Order { get; set; }
        public DateTime? Implementation_Date { get; set; }
        public bool? Status_Of_order { get; set; }
        public bool? Status_Of_Payment { get; set; }
    }
}
