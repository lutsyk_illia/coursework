﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_CarService.Model
{
    public class ServiceOrderInformation
    {
        public int Order_Number { get; set; }
        public int Serviceid;
        public string Service_Name { get; set; }
        public decimal? Service_Price { get; set; }
        public int? Execution_Time { get; set; }
        public string Material_Name { get; set; }
        public decimal? Material_Price { get; set; }
        public int? Number_Of_material { get; set; }
        public string Worker_Lastname { get; set; }
        public string Worker_Firstname { get; set; }
        public string Position_Name { get; set; }
    }
}
