﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Model;
using CourseWork_CarService.Views.Inserts;
using CourseWork_CarService.Views.Select;
using CourseWork_CarService.Views.Updates;

namespace CourseWork_CarService.Views
{
    /// <summary>
    /// Логика взаимодействия для DispatcherForm.xaml
    /// </summary>
    public partial class DispatcherForm : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public DispatcherForm()
        {
            InitializeComponent();
            Pricedate.Content += System.DateTime.Now.Date.ToString(CultureInfo.InvariantCulture);
        }

        void GetAllCars()
        {
            var inf = from car in contextEntities.Car
                join brand in contextEntities.CarBrand on car.Brand_ID equals brand.Brand_ID
                join producer in contextEntities.CarProducer on brand.CarProducer_ID equals producer.CarProducer_ID
                join color in contextEntities.Color on car.Color_ID equals color.Color_ID
                join year in contextEntities.YearOfProduction on car.Year_ID equals year.Year_ID
                join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                select new CarInformation()
                {
                    Car_id = car.Car_ID,
                    Brand = brand.Brand,
                    Color = color.ColorName,
                    CarNumber = car.CarNumber,
                    DateOfRegistration = car.DateOfRegistration,
                    EngineNumber = car.EngineNumber,
                    OwnerFirstName = client.FirstName,
                    OwnerLastName = client.LastName,
                    Producer = producer.CarProducer1,
                    Year = year.YearOfProduction1
                };
            CarsGrid.ItemsSource = inf.ToList();
        }

        void GetAllClients()
        {
            var result = from client in contextEntities.Client
                select new ClientInformation()
                {
                    Clientid = client.Client_ID,
                    Firstname = client.FirstName,
                    Lastname = client.LastName,
                    Homeaddress = client.HomeAddress,
                    Phone = client.Phone
                };
            ClientsGrid.ItemsSource = result.ToList();
        }

        void GetAllServiceOrders()
        {
            var result = from serviceOrder in contextEntities.ServiceOrder
                join service in contextEntities.Service on serviceOrder.Service_ID equals service.Service_ID
                join material in contextEntities.Material on serviceOrder.Material_ID equals material.Material_ID
                join worker in contextEntities.Worker on serviceOrder.Worker_ID equals worker.Worker_ID
                join position in contextEntities.Position on worker.Position_ID equals position.Position_ID
                select new ServiceOrderInformation
                {
                    Order_Number = serviceOrder.Order_ID,
                    Serviceid = serviceOrder.Service_ID,
                    Service_Name = service.ServiceName,
                    Service_Price = service.ServicePrice,
                    Execution_Time = service.NormalAmountOfTime,
                    Material_Name = material.MaterialName,
                    Material_Price = material.Price,
                    Number_Of_material = serviceOrder.NumberOfMaterial,
                    Worker_Lastname = worker.LastName,
                    Worker_Firstname = worker.FirsName,
                    Position_Name = position.PositionName
                };
            ServiceOrderGrid.ItemsSource = result.ToList();
        }

        void GetAllServices()
        {
            var result = from serv in contextEntities.Service
                select new ServiceInformation()
                {
                    Serviceid = serv.Service_ID,
                    ServiceName = serv.ServiceName,
                    ServicePrice = serv.ServicePrice,
                    ExecutionTime = serv.NormalAmountOfTime
                };
            ServiceGrid.ItemsSource = result.ToList();
        }

        void GetAllOrders()
        {
            var result = from order in contextEntities.Orders
                join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                select new OrderInformation()
                {
                    Order_Number = order.Order_ID,
                    Car_Number = car.CarNumber,
                    Client_Firstname = client.FirstName,
                    Client_Lastname = client.LastName,
                    Description = order.OrderDescription,
                    Price = order.OrderPrice,
                    Order_Discount = order.Discount,
                    Final_Price = order.PriceWithDiscount,
                    Date_Of_Order = order.DateOfOrder,
                    Implementation_Date = order.DateOfImplementation,
                    Status_Of_order = order.OrderStatus,
                    Status_Of_Payment = order.PaymentStatus
                };
            OrdersGrid.ItemsSource = result.ToList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }

        private void UpdateData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                new UpdateOrder(orders.Order_Number).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }


        private void DeleteData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from select_order in contextEntities.Orders
                             where orders.Order_Number == select_order.Order_ID
                             select select_order).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Orders.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertData_Click(object sender, RoutedEventArgs e)
        {
            new InsertOrder().ShowDialog();
            Reload();
        }

        private void DeleteServiceData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service = ServiceGrid.SelectedItem as ServiceInformation;
                var match = (from sel_service in contextEntities.Service
                             where service.Serviceid == sel_service.Service_ID
                             select sel_service).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Service.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateServiceData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service = ServiceGrid.SelectedItem as ServiceInformation;
                new UpdateService(service.Serviceid).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertServiceData_Click(object sender, RoutedEventArgs e)
        {
            new InsertService().ShowDialog();
            Reload();
        }

        private void InsertServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            new InsertServiceOrder().ShowDialog();
            Reload();
        }

        private void UpdateServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service_order = ServiceOrderGrid.SelectedItem as ServiceOrderInformation;
                new UpdateServiceOrder(service_order.Order_Number, service_order.Serviceid).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service_order = ServiceOrderGrid.SelectedItem as ServiceOrderInformation;
                var match = (from servord in contextEntities.ServiceOrder
                    where service_order.Order_Number == servord.Order_ID &&
                          servord.Service_ID == service_order.Serviceid
                    select servord).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.ServiceOrder.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteCarData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var car = CarsGrid.SelectedItem as CarInformation;
                var match = (from cars in contextEntities.Car
                    where cars.Car_ID == car.Car_id
                    select cars).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Car.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateCarData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var car = CarsGrid.SelectedItem as CarInformation;
                new UpdateCar(car.Car_id).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertCarsData_Click(object sender, RoutedEventArgs e)
        {
            new InsertCar().ShowDialog();
            Reload();
        }

        private void DeleteClientData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                var match = (from client1 in contextEntities.Client
                             where client.Clientid == client1.Client_ID
                             select client1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Client.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateClientData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                new UpdateClient(client.Clientid).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertClientsData_Click(object sender, RoutedEventArgs e)
        {
            new InsertClient().ShowDialog();
            Reload();
        }

        void Reload()
        {
            try
            {
                GetAllOrders();
                GetAllServices();
                GetAllServiceOrders();
                GetAllCars();
                GetAllClients();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        void GeneratePayCheck(int orderid)
        {
            var order = from vPayCheck in contextEntities.vPayCheck
                where vPayCheck.Order_ID == orderid
                select vPayCheck;
            string path = order.First().LastName + order.First().FirstName + ".txt";
            File.Create(path).Dispose();
            if (File.Exists(path))
            {
                TextWriter tw = new StreamWriter(path);
                tw.WriteLine("Номер замовлення - " + order.First().Order_ID + "\r\n");
                tw.WriteLine("Клієнт - " + order.First().LastName + " " + order.First().FirstName);
                tw.WriteLine("Автомобіль - " + order.First().CarProducer.Replace(" ","") + " " + order.First().Brand.Replace(" ", "") + " " +
                                order.First().CarNumber.Replace(" ", "") + "\r\n");
                tw.WriteLine("Послуги:\n");
                foreach (var vPayCheck in order)
                {
                    tw.WriteLine("\t" + vPayCheck.ServiceName + " ціна - " + vPayCheck.ServicePrice + "\r\n");
                    tw.WriteLine("\t\tМатеріал: " + vPayCheck.MaterialName + " ціна - " + vPayCheck.Price + "\r\n");
                }
                tw.WriteLine("Ціна - " + order.First().OrderPrice + " знижка - " + order.First().Discount +
                                " до оплати - " + order.First().PriceWithDiscount + "\r\n");
                tw.WriteLine("Дата виконання - " + order.First().DateOfImplementation);
                tw.Close();
            }
            File.Open(path, FileMode.Open);
        }

        private void Finished_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from contextEntitiesOrder in contextEntities.Orders
                             where orders.Order_Number == contextEntitiesOrder.Order_ID
                             select contextEntitiesOrder).SingleOrDefault();
                match.OrderStatus = true;
                match.DateOfImplementation = System.DateTime.Now;
                contextEntities.SaveChanges();
                GeneratePayCheck(orders.Order_Number);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Payed_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from contextEntitiesOrder in contextEntities.Orders
                             where orders.Order_Number == contextEntitiesOrder.Order_ID
                             select contextEntitiesOrder).SingleOrDefault();
                match.PaymentStatus = true;
                contextEntities.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void FinishedOrders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = from order in contextEntities.Orders
                             join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                             join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                             where order.OrderStatus == true
                             select new OrderInformation()
                             {
                                 Order_Number = order.Order_ID,
                                 Car_Number = car.CarNumber,
                                 Client_Firstname = client.FirstName,
                                 Client_Lastname = client.LastName,
                                 Description = order.OrderDescription,
                                 Price = order.OrderPrice,
                                 Order_Discount = order.Discount,
                                 Final_Price = order.PriceWithDiscount,
                                 Date_Of_Order = order.DateOfOrder,
                                 Implementation_Date = order.DateOfImplementation,
                                 Status_Of_order = order.OrderStatus,
                                 Status_Of_Payment = order.PaymentStatus
                             };
                OrdersGrid.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UnFinishedOrders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = from order in contextEntities.Orders
                             join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                             join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                             where order.OrderStatus == false
                             select new OrderInformation()
                             {
                                 Order_Number = order.Order_ID,
                                 Car_Number = car.CarNumber,
                                 Client_Firstname = client.FirstName,
                                 Client_Lastname = client.LastName,
                                 Description = order.OrderDescription,
                                 Price = order.OrderPrice,
                                 Order_Discount = order.Discount,
                                 Final_Price = order.PriceWithDiscount,
                                 Date_Of_Order = order.DateOfOrder,
                                 Implementation_Date = order.DateOfImplementation,
                                 Status_Of_order = order.OrderStatus,
                                 Status_Of_Payment = order.PaymentStatus
                             };
                OrdersGrid.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void SelectUserOrders_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                new SelectOrderByUser(client.Clientid, client.Firstname + " " + client.Lastname).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Ordersbydate(object sender, RoutedEventArgs e)
        {
            new SelectOrderByDate().ShowDialog();
        }
    }
}
