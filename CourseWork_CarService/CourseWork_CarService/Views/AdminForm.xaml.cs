﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Model;
using CourseWork_CarService.Views.Inserts;
using CourseWork_CarService.Views.Select;
using CourseWork_CarService.Views.Updates;

namespace CourseWork_CarService.Views
{
    /// <summary>
    /// Логика взаимодействия для AdminForm.xaml
    /// </summary>
    public partial class AdminForm : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public AdminForm()
        {
            InitializeComponent();
            var users = contextEntities.WPFUsers.ToList();
            UserGrid.ItemsSource = users;
            Pricedate.Content += System.DateTime.Now.Date.ToString(CultureInfo.InvariantCulture);
        }

        void GetAllProducers()
        {
            var inf = from carProducer in contextEntities.CarProducer
                select new CarProducerInformation
                {
                    CarProducerId = carProducer.CarProducer_ID,
                    CarProducer_Name = carProducer.CarProducer1
                };
            ProducerGrid.ItemsSource = inf.ToList();
        }

        void GetAllBrands()
        {
            var inf = from carBrand in contextEntities.CarBrand
                join carProducer in contextEntities.CarProducer on carBrand.CarProducer_ID equals carProducer
                    .CarProducer_ID
                select new CarBrandInformation()
                {
                    CarBrandId = carBrand.Brand_ID,
                    Brand = carBrand.Brand,
                    CarProducer_Name = carProducer.CarProducer1
                };
            BrandGrid.ItemsSource = inf.ToList();
        }

        void GetAllCars()
        {
            var inf = from car in contextEntities.Car
                      join brand in contextEntities.CarBrand on car.Brand_ID equals brand.Brand_ID
                      join producer in contextEntities.CarProducer on brand.CarProducer_ID equals producer.CarProducer_ID
                      join color in contextEntities.Color on car.Color_ID equals color.Color_ID
                      join year in contextEntities.YearOfProduction on car.Year_ID equals year.Year_ID
                      join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                      select new CarInformation()
                      {
                          Car_id = car.Car_ID,
                          Brand = brand.Brand,
                          Color = color.ColorName,
                          CarNumber = car.CarNumber,
                          DateOfRegistration = car.DateOfRegistration,
                          EngineNumber = car.EngineNumber,
                          OwnerFirstName = client.FirstName,
                          OwnerLastName = client.LastName,
                          Producer = producer.CarProducer1,
                          Year = year.YearOfProduction1
                      };
            CarsGrid.ItemsSource = inf.ToList();
        }

        void GetAllClients()
        {
            var result = from client in contextEntities.Client
                         select new ClientInformation()
                         {
                             Clientid = client.Client_ID,
                             Firstname = client.FirstName,
                             Lastname = client.LastName,
                             Homeaddress = client.HomeAddress,
                             Phone = client.Phone
                         };
            ClientsGrid.ItemsSource = result.ToList();
        }

        void GetAllServiceOrders()
        {
            var result = from serviceOrder in contextEntities.ServiceOrder
                         join service in contextEntities.Service on serviceOrder.Service_ID equals service.Service_ID
                         join material in contextEntities.Material on serviceOrder.Material_ID equals material.Material_ID
                         join worker in contextEntities.Worker on serviceOrder.Worker_ID equals worker.Worker_ID
                         join position in contextEntities.Position on worker.Position_ID equals position.Position_ID
                         select new ServiceOrderInformation
                         {
                             Order_Number = serviceOrder.Order_ID,
                             Serviceid = serviceOrder.Service_ID,
                             Service_Name = service.ServiceName,
                             Service_Price = service.ServicePrice,
                             Execution_Time = service.NormalAmountOfTime,
                             Material_Name = material.MaterialName,
                             Material_Price = material.Price,
                             Number_Of_material = serviceOrder.NumberOfMaterial,
                             Worker_Lastname = worker.LastName,
                             Worker_Firstname = worker.FirsName,
                             Position_Name = position.PositionName
                         };
            ServiceOrderGrid.ItemsSource = result.ToList();
        }

        void GetAllServices()
        {
            var result = from serv in contextEntities.Service
                         select new ServiceInformation()
                         {
                             Serviceid = serv.Service_ID,
                             ServiceName = serv.ServiceName,
                             ServicePrice = serv.ServicePrice,
                             ExecutionTime = serv.NormalAmountOfTime
                         };
            ServiceGrid.ItemsSource = result.ToList();
        }

        void GetAllOrders()
        {
            var result = from order in contextEntities.Orders
                         join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                         join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                         select new OrderInformation()
                         {
                             Order_Number = order.Order_ID,
                             Car_Number = car.CarNumber,
                             Client_Firstname = client.FirstName,
                             Client_Lastname = client.LastName,
                             Description = order.OrderDescription,
                             Price = order.OrderPrice,
                             Order_Discount = order.Discount,
                             Final_Price = order.PriceWithDiscount,
                             Date_Of_Order = order.DateOfOrder,
                             Implementation_Date = order.DateOfImplementation,
                             Status_Of_order = order.OrderStatus,
                             Status_Of_Payment = order.PaymentStatus
                         };
            OrdersGrid.ItemsSource = result.ToList();
        }       

        private void InsertUser_Click(object sender, RoutedEventArgs e)
        {
            if (Username.Text != "" && Password.Text != "" && userType.SelectedIndex!=-1)
            {
                try
                {
                    WPFUsers createdUser = new WPFUsers()
                    {
                        password = Password.Text,
                        username = Username.Text,
                        user_type = (userType.SelectedValue as TextBlock).Text
                    };
                    var user = (from users in contextEntities.WPFUsers
                                where users.username == createdUser.username
                                select users).SingleOrDefault();
                    if (user == null)
                    {
                        contextEntities.WPFUsers.Add(createdUser);
                        contextEntities.SaveChanges();
                        var users = contextEntities.WPFUsers.ToList();
                        UserGrid.ItemsSource = users;
                    }
                    else
                    {
                        MessageBox.Show("Користувач з таким іменем вже існує", "Помилка");
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Перевірте введення даних", "Помилка");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }

        private void UpdateData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                new UpdateOrder(orders.Order_Number).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }


        private void DeleteData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from select_order in contextEntities.Orders
                             where orders.Order_Number == select_order.Order_ID
                             select select_order).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Orders.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertData_Click(object sender, RoutedEventArgs e)
        {
            new InsertOrder().ShowDialog();
            Reload();
        }

        private void DeleteServiceData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service = ServiceGrid.SelectedItem as ServiceInformation;
                var match = (from sel_service in contextEntities.Service
                             where service.Serviceid == sel_service.Service_ID
                             select sel_service).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Service.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateServiceData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service = ServiceGrid.SelectedItem as ServiceInformation;
                new UpdateService(service.Serviceid).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }

        private void InsertServiceData_Click(object sender, RoutedEventArgs e)
        {
            new InsertService().ShowDialog();
            Reload();
        }

        private void InsertServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            new InsertServiceOrder().ShowDialog();
            Reload();
        }

        private void UpdateServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service_order = ServiceOrderGrid.SelectedItem as ServiceOrderInformation;
                new UpdateServiceOrder(service_order.Order_Number, service_order.Serviceid).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }

        private void DeleteServiceOrderData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var service_order = ServiceOrderGrid.SelectedItem as ServiceOrderInformation;
                var match = (from servord in contextEntities.ServiceOrder
                             where service_order.Order_Number == servord.Order_ID &&
                                   servord.Service_ID == service_order.Serviceid
                             select servord).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.ServiceOrder.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteCarData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var car = CarsGrid.SelectedItem as CarInformation;
                var match = (from cars in contextEntities.Car
                             where cars.Car_ID == car.Car_id
                             select cars).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Car.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateCarData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var car = CarsGrid.SelectedItem as CarInformation;
                new UpdateCar(car.Car_id).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }

        private void InsertCarsData_Click(object sender, RoutedEventArgs e)
        {
            new InsertCar().ShowDialog();
            Reload();
        }

        private void DeleteClientData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                var match = (from client1 in contextEntities.Client
                             where client.Clientid == client1.Client_ID
                             select client1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Client.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateClientData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                new UpdateClient(client.Clientid).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }

        private void InsertClientsData_Click(object sender, RoutedEventArgs e)
        {
            new InsertClient().ShowDialog();
            Reload();
        }

        void Reload()
        {
            try
            {
                GetAllOrders();
                GetAllServices();
                GetAllServiceOrders();
                GetAllCars();
                GetAllClients();
                GetAllBrands();
                GetAllProducers();
                GetAllPositions();
                GetAllWorkers();
                GetAllMaterials();
                GetAllYears();
                GetAllColors();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        private void Finished_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from contextEntitiesOrder in contextEntities.Orders
                             where orders.Order_Number == contextEntitiesOrder.Order_ID
                             select contextEntitiesOrder).SingleOrDefault();
                match.OrderStatus = true;
                match.DateOfImplementation = System.DateTime.Now;
                contextEntities.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Payed_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var orders = OrdersGrid.SelectedItem as OrderInformation;
                var match = (from contextEntitiesOrder in contextEntities.Orders
                             where orders.Order_Number == contextEntitiesOrder.Order_ID
                             select contextEntitiesOrder).SingleOrDefault();
                match.PaymentStatus = true;
                contextEntities.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void FinishedOrders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = from order in contextEntities.Orders
                             join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                             join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                             where order.OrderStatus == true
                             select new OrderInformation()
                             {
                                 Order_Number = order.Order_ID,
                                 Car_Number = car.CarNumber,
                                 Client_Firstname = client.FirstName,
                                 Client_Lastname = client.LastName,
                                 Description = order.OrderDescription,
                                 Price = order.OrderPrice,
                                 Order_Discount = order.Discount,
                                 Final_Price = order.PriceWithDiscount,
                                 Date_Of_Order = order.DateOfOrder,
                                 Implementation_Date = order.DateOfImplementation,
                                 Status_Of_order = order.OrderStatus,
                                 Status_Of_Payment = order.PaymentStatus
                             };
                OrdersGrid.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UnFinishedOrders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = from order in contextEntities.Orders
                             join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                             join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                             where order.OrderStatus == false
                             select new OrderInformation()
                             {
                                 Order_Number = order.Order_ID,
                                 Car_Number = car.CarNumber,
                                 Client_Firstname = client.FirstName,
                                 Client_Lastname = client.LastName,
                                 Description = order.OrderDescription,
                                 Price = order.OrderPrice,
                                 Order_Discount = order.Discount,
                                 Final_Price = order.PriceWithDiscount,
                                 Date_Of_Order = order.DateOfOrder,
                                 Implementation_Date = order.DateOfImplementation,
                                 Status_Of_order = order.OrderStatus,
                                 Status_Of_Payment = order.PaymentStatus
                             };
                OrdersGrid.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertBrandData_Click(object sender, RoutedEventArgs e)
        {
            new InsertCarBrand().ShowDialog();
            Reload();
        }

        private void UpdateBrandData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var brand = BrandGrid.SelectedItem as CarBrandInformation;
                new UpdateCarBrand(brand.CarBrandId).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
            Reload();
        }

        private void DeleteBrandData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var brand = BrandGrid.SelectedItem as CarBrandInformation;
                var sel_brand = (from carBrand in contextEntities.CarBrand
                                 where carBrand.Brand_ID == brand.CarBrandId
                                 select carBrand).SingleOrDefault();
                if (sel_brand != null)
                {
                    contextEntities.CarBrand.Remove(sel_brand);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteProducerData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var producer = ProducerGrid.SelectedItem as CarProducerInformation;
                var match = (from carProducer in contextEntities.CarProducer
                             where carProducer.CarProducer_ID == producer.CarProducerId
                             select carProducer).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.CarProducer.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void UpdateProducerData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var producer = ProducerGrid.SelectedItem as CarProducerInformation;
                new UpdateCarProducer(producer.CarProducerId).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertProducerData_Click(object sender, RoutedEventArgs e)
        {
            new InsertCarProducer().ShowDialog();
            Reload();
        }

        void GetAllPositions()
        {
            var result = from position in contextEntities.Position
                select new PositionInformation()
                {
                    PositionId = position.Position_ID,
                    PositionName = position.PositionName
                };
            PositionGrid.ItemsSource = result.ToList();
        }

        private void InsertPositionData_Click(object sender, RoutedEventArgs e)
        {
            new InsertPosition().ShowDialog();
            Reload();
        }

        private void UpdatePositionData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var position = PositionGrid.SelectedItem as PositionInformation;
                new UpdatePosition(position.PositionId).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeletePositionData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var position = PositionGrid.SelectedItem as PositionInformation;
                var match = (from position1 in contextEntities.Position
                             where position1.Position_ID == position.PositionId
                             select position1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Position.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        void GetAllWorkers()
        {
            var result = from worker in contextEntities.Worker
                join position in contextEntities.Position on worker.Position_ID equals position.Position_ID
                select new WorkerInformation()
                {
                    Workerid = worker.Worker_ID,
                    Firstname = worker.FirsName,
                    Lastname = worker.LastName,
                    PositionName = position.PositionName
                };
            WorkerGrid.ItemsSource = result.ToList();
        }

        private void InsertWorkerData_Click(object sender, RoutedEventArgs e)
        {
            new InsertWorker().ShowDialog();
            Reload();
        }

        private void UpdateWorkerData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var worker = WorkerGrid.SelectedItem as WorkerInformation;
                new UpdateWorker(worker.Workerid).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteWorkerData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var worker = WorkerGrid.SelectedItem as WorkerInformation;
                var match = (from worker1 in contextEntities.Worker
                             where worker1.Worker_ID == worker.Workerid
                             select worker1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Worker.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        void GetAllMaterials()
        {
            var result = from material in contextEntities.Material
                select new MaterialInformation()
                {
                    MaterialId = material.Material_ID,
                    MaterialName = material.MaterialName,
                    Price = material.Price,
                    DateOfIssue = material.DateOfIssue
                };
            MaterialGrid.ItemsSource = result.ToList();
        }

        private void InsertMaterialData_Click(object sender, RoutedEventArgs e)
        {
            new InsertMaterial().ShowDialog();
            Reload();
        }

        private void UpdateMaterialData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var material = MaterialGrid.SelectedItem as MaterialInformation;
                new UpdateMaterial(material.MaterialId).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteMaterialData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var material = MaterialGrid.SelectedItem as MaterialInformation;
                var match = (from material1 in contextEntities.Material
                             where material1.Material_ID == material.MaterialId
                             select material1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Material.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertYearData_Click(object sender, RoutedEventArgs e)
        {
            new InsertYear().ShowDialog();
            Reload();
        }

        private void UpdateYearData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var year = YearGrid.SelectedItem as YearProductionInformation;
                new UpdateYear(year.YearID).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteYearData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var year = YearGrid.SelectedItem as YearProductionInformation;
                var match = (from yearOfProduction in contextEntities.YearOfProduction
                             where yearOfProduction.Year_ID == year.YearID
                             select yearOfProduction).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.YearOfProduction.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void InsertColorData_Click(object sender, RoutedEventArgs e)
        {
            new InsertColor().ShowDialog();
            Reload();
        }

        private void UpdateColorData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var color = ColorGrid.SelectedItem as ColorInformation;
                new UpdateColor(color.Colorid).ShowDialog();
                Reload();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void DeleteColorData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var color = ColorGrid.SelectedItem as ColorInformation;
                var match = (from color1 in contextEntities.Color
                             where color1.Color_ID == color.Colorid
                             select color1).SingleOrDefault();
                if (match != null)
                {
                    contextEntities.Color.Remove(match);
                    contextEntities.SaveChanges();
                    Reload();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        void GetAllYears()
        {
            var result = from yearOfProduction in contextEntities.YearOfProduction
                select new YearProductionInformation
                {
                    YearID = yearOfProduction.Year_ID,
                    Year = yearOfProduction.YearOfProduction1
                };
            YearGrid.ItemsSource = result.ToList();
        }

        void GetAllColors()
        {
            var result = from color in contextEntities.Color
                select new ColorInformation()
                {
                    Colorid = color.Color_ID,
                    Color_Name = color.ColorName
                };
            ColorGrid.ItemsSource = result.ToList();
        }

        private void SelectUserOrders_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = ClientsGrid.SelectedItem as ClientInformation;
                new SelectOrderByUser(client.Clientid, client.Firstname + " " + client.Lastname).ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Ordersbydate(object sender, RoutedEventArgs e)
        {
            new SelectOrderByDate().ShowDialog();
        }
    }
}
