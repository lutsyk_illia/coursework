﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Views.Inserts;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateWorker.xaml
    /// </summary>
    public partial class UpdateWorker : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        private int WorkerID;
        public UpdateWorker(int workerid)
        {
            InitializeComponent();
            WorkerID = workerid;
            try
            {
                var result = from position in contextEntities.Position
                             select position.PositionName;
                PositionBox.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void AddPosition_Click(object sender, RoutedEventArgs e)
        {
            new InsertPosition().ShowDialog();
        }

        private void UpdateWorker_Click(object sender, RoutedEventArgs e)
        {
            if (PositionBox.SelectedIndex != -1 && WorkerLasname.Text != "" && WorkerFirstname.Text != "")
            {
                try
                {
                    try
                    {
                        var positionid = (from posid in contextEntities.Position
                                          where posid.PositionName == PositionBox.Text
                                          select posid.Position_ID).SingleOrDefault();
                        var worker = (from work in contextEntities.Worker
                                      where work.Worker_ID == WorkerID
                                      select work).SingleOrDefault();
                        if (worker != null)
                        {
                            worker.LastName = WorkerLasname.Text;
                            worker.FirsName = WorkerFirstname.Text;
                            worker.Position_ID = positionid;
                        }
                        contextEntities.SaveChanges();
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "Error");
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
