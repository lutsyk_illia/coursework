﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateMaterial.xaml
    /// </summary>
    public partial class UpdateMaterial : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        private int MaterialID;
        public UpdateMaterial(int materialid)
        {
            InitializeComponent();
            MaterialID = materialid;
        }

        private void Update_OnClick(object sender, RoutedEventArgs e)
        {
            if (MaterialName.Text != "" && MaterialPrice.Text != "" && DatePicker.Text != "")
            {
                try
                {
                    var material = (from material1 in contextEntities.Material
                        where material1.Material_ID == MaterialID
                        select material1).SingleOrDefault();
                    if (material != null)
                    {
                        material.MaterialName = MaterialName.Text;
                        material.Price = decimal.Parse(MaterialPrice.Text);
                        material.DateOfIssue = DatePicker.SelectedDate;
                    }
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Warnings");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warnings");
            }
        }
    }
}
