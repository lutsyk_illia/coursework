﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateCarProducer.xaml
    /// </summary>
    public partial class UpdateCarProducer : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        private int ProducerID;
        public UpdateCarProducer(int producerId)
        {
            InitializeComponent();
            ProducerID = producerId;
        }

        private void Insert_OnClick(object sender, RoutedEventArgs e)
        {
            if (ProducerText.Text != "")
            {
                try
                {
                    var producer = (from carProducer in contextEntities.CarProducer
                                    where carProducer.CarProducer_ID == ProducerID
                                    select carProducer).SingleOrDefault();
                    producer.CarProducer1 = ProducerText.Text;
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warnings");
            }
        }
    }
}
