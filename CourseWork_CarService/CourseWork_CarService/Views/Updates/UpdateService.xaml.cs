﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateService.xaml
    /// </summary>
    public partial class UpdateService : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        private int ServiceID;
        public UpdateService(int serviceId)
        {
            InitializeComponent();
            ServiceID = serviceId;
        }

        private void Update_OnClick_Click(object sender, RoutedEventArgs e)
        {
            if (ServiceName.Text != "" && ServicePrice.Text != "" && ExecutionTime.Text != "")
            {
                try
                {
                    var service = (from serv in context.Service
                                   where serv.Service_ID == ServiceID
                                   select serv).SingleOrDefault();
                    service.ServiceName = ServiceName.Text;
                    service.ServicePrice = decimal.Parse(ServicePrice.Text);
                    service.NormalAmountOfTime = int.Parse(ExecutionTime.Text);
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
