﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateCar.xaml
    /// </summary>
    public partial class UpdateCar : Window
    {
        private int carID;
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public UpdateCar(int carid)
        {
            InitializeComponent();
            try
            {
                carID = carid;
                var result_brand = from brand in context.CarBrand
                                   select brand.Brand;
                Brandbox.ItemsSource = result_brand.ToList();
                var result_year = from year in context.YearOfProduction
                                  select year.YearOfProduction1;
                Yearbox.ItemsSource = result_year.ToList();
                var result_color = from color in context.Color
                                   select color.ColorName;
                Colorbox.ItemsSource = result_color.ToList();
                var result_client = from clients in context.Client
                                    select clients.LastName + " " + clients.FirstName + " " + clients.Phone;
                Clientbox.ItemsSource = result_client.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Update_OnClick(object sender, RoutedEventArgs e)
        {
            if (DatePicker.Text != "" && Brandbox.SelectedIndex != -1 && Colorbox.SelectedIndex != -1 &&
                Yearbox.SelectedIndex != -1 && Clientbox.SelectedIndex != -1 && EngineNumber.Text != "" &&
                CarNumber.Text != "")
            {
                try
                {
                    int yearbox = int.Parse(Yearbox.Text);
                    var brand = (from arg in context.CarBrand
                                 where arg.Brand == Brandbox.Text
                                 select arg.Brand_ID).SingleOrDefault();
                    var year = (from arg in context.YearOfProduction
                                where arg.YearOfProduction1 == yearbox
                                select arg.Year_ID).SingleOrDefault();
                    var color = (from arg in context.Color
                                 where arg.ColorName == Colorbox.Text
                                 select arg.Color_ID).SingleOrDefault();
                    var client_mas = Clientbox.Text.Split(' ');
                    string firstname, lastname, phone, homeaddress;
                    lastname = client_mas[0];
                    firstname = client_mas[1];
                    phone = client_mas[2];
                    var client = (from arg in context.Client
                                  where arg.LastName == lastname && arg.FirstName == firstname && arg.Phone == phone
                                  select arg.Client_ID).First();
                    var car = (from cars in context.Car
                        where cars.Car_ID == carID
                        select cars).SingleOrDefault();
                    car.Brand_ID = brand;
                    car.Client_ID = client;
                    car.Color_ID = color;
                    car.Year_ID = year;
                    car.CarNumber = CarNumber.Text;
                    car.EngineNumber = EngineNumber.Text;
                    car.DateOfRegistration = DatePicker.SelectedDate;
                    context.SaveChanges();

                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
