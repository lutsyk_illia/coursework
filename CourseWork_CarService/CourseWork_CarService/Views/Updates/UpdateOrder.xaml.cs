﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateOrder.xaml
    /// </summary>
    public partial class UpdateOrder : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        private int OrderID;
        public UpdateOrder(int orderid)
        {
            InitializeComponent();
            OrderID = orderid;
            try
            {
                var result_car = from car in context.Car
                    select car.CarNumber;
                NumberBox.ItemsSource = result_car.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Update_OnClick(object sender, RoutedEventArgs e)
        {
            string desc1 = new TextRange(Description.Document.ContentStart, Description.Document.ContentEnd).Text;
            string desc = desc1.Replace('.', ',');
            if (NumberBox.SelectedIndex != -1 && desc != "" && DiscountText.Text != "" &&
                DatePicker.Text != "")
            {
                try
                {
                    var car = (from arg in context.Car
                        where arg.CarNumber == NumberBox.Text
                        select arg.Car_ID).FirstOrDefault();
                    var order = (from arg in context.Orders
                        where arg.Order_ID == OrderID
                        select arg).SingleOrDefault();
                    if (order != null)
                    {
                        order.Car_ID = car;
                        order.OrderDescription = desc;
                        order.Discount = decimal.Parse(DiscountText.Text);
                        order.DateOfOrder = DatePicker.SelectedDate;
                    }
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
