﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Model;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateCarBrand.xaml
    /// </summary>
    public partial class UpdateCarBrand : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        private int BrandID;
        public UpdateCarBrand(int brandid)
        {
            InitializeComponent();
            BrandID = brandid;
            try
            {
                var result = from prod in contextEntities.CarProducer
                             select prod.CarProducer1;
                ProducerBox.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Update_OnClick_OnClick(object sender, RoutedEventArgs e)
        {
            if (BrandText.Text != "" && ProducerBox.SelectedIndex != -1)
            {
                try
                {
                    var brand = (from brands in contextEntities.CarBrand
                                 where brands.Brand_ID == BrandID
                                 select brands).SingleOrDefault();
                    var prod = (from carProducer in contextEntities.CarProducer
                                where carProducer.CarProducer1 == ProducerBox.Text
                                select carProducer.CarProducer_ID).SingleOrDefault();
                    if (brand != null)
                    {
                        brand.CarProducer_ID = prod;
                        brand.Brand = BrandText.Text;
                    }
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warnings");
            }
        }
    }
}
