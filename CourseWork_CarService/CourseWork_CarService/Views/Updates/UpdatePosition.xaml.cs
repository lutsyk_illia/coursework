﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdatePosition.xaml
    /// </summary>
    public partial class UpdatePosition : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        private int PositionID;
        public UpdatePosition(int positionId)
        {
            InitializeComponent();
            PositionID = positionId;
        }

        private void Update_OnClick(object sender, RoutedEventArgs e)
        {
            if (PositionText.Text != "")
            {
                try
                {
                    var posit = (from position in contextEntities.Position
                                 where position.Position_ID == PositionID
                                 select position).SingleOrDefault();
                    posit.PositionName = PositionText.Text;
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Повідомлення");
            }
        }
    }
}
