﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateYear.xaml
    /// </summary>
    public partial class UpdateYear : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        private int YearID;
        public UpdateYear(int yearid)
        {
            InitializeComponent();
            YearID = yearid;
        }

        private void UpdateYear_Click(object sender, RoutedEventArgs e)
        {
            if (YearText.Text != "")
            {
                try
                {
                    var year = (from years in context.YearOfProduction
                                where years.Year_ID == YearID
                                select years).SingleOrDefault();
                    year.YearOfProduction1 = int.Parse(YearText.Text);
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Повідомлення");
            }
        }
    }
}
