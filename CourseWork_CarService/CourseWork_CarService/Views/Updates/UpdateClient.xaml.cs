﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Updates
{
    /// <summary>
    /// Логика взаимодействия для UpdateClient.xaml
    /// </summary>
    public partial class UpdateClient : Window
    {
        private int clientID;
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public UpdateClient(int clientid)
        {
            InitializeComponent();
            clientID = clientid;
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (Firstname.Text != "" && Lastname.Text != "" && Homeaddress.Text != "" && Phone.Text != "")
            {

                try
                {
                    var client = (from clients in context.Client
                                  where clients.Client_ID == clientID
                                  select clients).SingleOrDefault();
                    if (client != null)
                    {
                        client.FirstName = Firstname.Text;
                        client.LastName = Lastname.Text;
                        client.HomeAddress = Homeaddress.Text;
                        client.Phone = Phone.Text;
                        context.SaveChanges();
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
