﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertMaterial.xaml
    /// </summary>
    public partial class InsertMaterial : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public InsertMaterial()
        {
            InitializeComponent();
        }

        private void Insert_OnClick(object sender, RoutedEventArgs e)
        {
            if (MaterialName.Text != "" && MaterialPrice.Text != "" && DatePicker.Text != "")
            {
                try
                {
                    contextEntities.Material.Add(new Material()
                    {
                        DateOfIssue = DatePicker.SelectedDate,
                        MaterialName = MaterialName.Text,
                        Price = decimal.Parse(MaterialPrice.Text)
                    });
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Warnings");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warnings");
            }
        }
    }
}
