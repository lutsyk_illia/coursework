﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertOrder.xaml
    /// </summary>
    public partial class InsertOrder : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public InsertOrder()
        {
            InitializeComponent();
            try
            {
                var result_car = from car in context.Car
                    select car.CarNumber;
                NumberBox.ItemsSource = result_car.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Insert_Click(object sender, RoutedEventArgs e)
        {
            string desc1 = new TextRange(Description.Document.ContentStart,Description.Document.ContentEnd).Text;
            string desc = desc1.Replace('.', ',');
            if (NumberBox.SelectedIndex != -1 &&  desc != "" && DiscountText.Text != "" &&
                DatePicker.Text != "")
            {
                try
                {
                    var car = (from arg in context.Car
                               where arg.CarNumber == NumberBox.Text
                               select arg.Car_ID).FirstOrDefault();
                    context.Orders.Add(new Orders()
                    {
                        Car_ID = car,
                        DateOfOrder = DatePicker.SelectedDate,
                        Discount = decimal.Parse(DiscountText.Text),
                        OrderDescription = desc,
                        OrderStatus = false,
                        PaymentStatus = false,
                        OrderPrice = 0,
                        PriceWithDiscount = 0
                    });
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
