﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertService.xaml
    /// </summary>
    public partial class InsertService : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public InsertService()
        {
            InitializeComponent();
        }

        private void Insert_OnClick_Click(object sender, RoutedEventArgs e)
        {
            if (ServiceName.Text != "" && ServicePrice.Text != "" && ExecutionTime.Text != "")
            {
                try
                {
                    //context.InsertNewService(ServiceName.Text, decimal.Parse(ServicePrice.Text),
                    //    int.Parse(ExecutionTime.Text));
                    Service service = new Service()
                    {
                        ServiceName = ServiceName.Text,
                        ServicePrice = decimal.Parse(ServicePrice.Text),
                        NormalAmountOfTime = int.Parse(ExecutionTime.Text)
                    };
                    context.Service.Add(service);
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
