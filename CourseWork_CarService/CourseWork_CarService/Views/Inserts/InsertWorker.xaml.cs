﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertWorker.xaml
    /// </summary>
    public partial class InsertWorker : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public InsertWorker()
        {
            InitializeComponent();
            try
            {
                var result = from position in contextEntities.Position
                             select position.PositionName;
                PositionBox.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void AddPosition_Click(object sender, RoutedEventArgs e)
        {
            new InsertPosition().ShowDialog();
        }

        private void AddWorker_Click(object sender, RoutedEventArgs e)
        {
            if (PositionBox.SelectedIndex != -1 && WorkerLasname.Text != "" && WorkerFirstname.Text != "")
            {
                try
                {
                    var positionid = (from posid in contextEntities.Position
                        where posid.PositionName == PositionBox.Text
                        select posid.Position_ID).SingleOrDefault();
                    contextEntities.Worker.Add(new Worker()
                    {
                        FirsName = WorkerFirstname.Text,
                        Position_ID = positionid,
                        LastName = WorkerLasname.Text
                    });
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
