﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertServiceOrder.xaml
    /// </summary>
    public partial class InsertServiceOrder : Window
    {
        private CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public InsertServiceOrder()
        {
            InitializeComponent();
            try
            {
                var orders = from order in context.Orders
                             where order.OrderStatus == false
                             select order.Order_ID;
                OrderNumber.ItemsSource = orders.ToList();
                var services = from service in context.Service
                               select service.ServiceName;
                ServiceName.ItemsSource = services.ToList();
                var materials = from material in context.Material
                                select material.MaterialName;
                MaterialName.ItemsSource = materials.ToList();
                var workers = from worker in context.Worker
                              join position in context.Position on worker.Position_ID equals position.Position_ID
                              select worker.LastName + " " + worker.FirsName + " " + position.PositionName;
                Worker.ItemsSource = workers.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        private void Insert_OnClick(object sender, RoutedEventArgs e)
        {
            if (OrderNumber.SelectedIndex != -1 && ServiceName.SelectedIndex != -1 &&
                MaterialName.SelectedIndex != -1 && Quantity.Text != "" && Worker.SelectedIndex != -1)
            {
                try
                {
                    var order = int.Parse(OrderNumber.Text);
                    var service = (from service1 in context.Service
                                   where service1.ServiceName == ServiceName.Text
                                   select service1.Service_ID).FirstOrDefault();
                    var material = (from materials in context.Material
                                    where materials.MaterialName == MaterialName.Text
                                    select materials.Material_ID).FirstOrDefault();
                    var quantity = int.Parse(Quantity.Text);
                    var work_inf = Worker.Text.Split(' ');
                    var surname = work_inf[0];
                    var name = work_inf[1];
                    var pos = work_inf[2];
                    var worker = (from workers in context.Worker
                                  join position in context.Position on workers.Position_ID equals position.Position_ID
                                  where workers.LastName == surname && workers.FirsName == name && position.PositionName == pos
                                  select workers.Worker_ID).FirstOrDefault();
                    context.ServiceOrder.Add(new ServiceOrder()
                    {
                        Order_ID = order,
                        Service_ID = service,
                        Material_ID = material,
                        NumberOfMaterial = quantity,
                        Worker_ID = worker
                    });
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
