﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Model;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertCarProducer.xaml
    /// </summary>
    public partial class InsertCarProducer : Window
    {
        private CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public InsertCarProducer()
        {
            InitializeComponent();
        }

        private void Insert_OnClick(object sender, RoutedEventArgs e)
        {
            if (ProducerText.Text != "")
            {
                try
                {
                    contextEntities.CarProducer.Add(new CarProducer()
                    {
                        CarProducer1 = ProducerText.Text
                    });
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warnings");
            }
        }
    }
}
