﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertPosition.xaml
    /// </summary>
    public partial class InsertPosition : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public InsertPosition()
        {
            InitializeComponent();
        }

        private void AddPosition_Click(object sender, RoutedEventArgs e)
        {
            if (PositionText.Text != "")
            {
                try
                {
                    Position position = new Position() { PositionName = PositionText.Text };
                    contextEntities.Position.Add(position);
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Повідомлення");
            }
        }
    }
}
