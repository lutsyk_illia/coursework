﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertClient.xaml
    /// </summary>
    public partial class InsertClient : Window
    {
        CarService_CourseWorkEntities context = new CarService_CourseWorkEntities();
        public InsertClient()
        {
            InitializeComponent();
        }

        private void Insert_OnClick_Click(object sender, RoutedEventArgs e)
        {
            if (Firstname.Text != "" && Lastname.Text != "" && Homeaddress.Text != "" && Phone.Text != "")
            {
                try
                {
                    Client client = new Client()
                    {
                        FirstName = Firstname.Text,
                        LastName = Lastname.Text,
                        HomeAddress = Homeaddress.Text,
                        Phone = Phone.Text
                    };
                    context.Client.Add(client);
                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter all fields", "Warning");
            }
        }
    }
}
