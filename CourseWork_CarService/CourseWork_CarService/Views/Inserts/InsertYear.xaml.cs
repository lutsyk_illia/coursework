﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views.Inserts
{
    /// <summary>
    /// Логика взаимодействия для InsertYear.xaml
    /// </summary>
    public partial class InsertYear : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public InsertYear()
        {
            InitializeComponent();
        }

        private void AddYear_Click(object sender, RoutedEventArgs e)
        {
            if (YearText.Text != "")
            {
                try
                {
                    YearOfProduction year = new YearOfProduction() { YearOfProduction1 = int.Parse(YearText.Text) };
                    contextEntities.YearOfProduction.Add(year);
                    contextEntities.SaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Повідомлення");
            }
        }
    }
}
