﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Views;

namespace CourseWork_CarService.Views
{
    /// <summary>
    /// Логика взаимодействия для LoginForm.xaml
    /// </summary>
    public partial class LoginForm : Window
    {
        CarService_CourseWorkEntities carServiceCourseWorkEntities = new CarService_CourseWorkEntities();
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            bool successful = false;
            foreach (var user in carServiceCourseWorkEntities.WPFUsers)
            {
                if (user.username != Username.Text || user.password != UserPassword.Password) continue;
                successful = true;
                if (user.user_type == "Адмін")
                {
                    AdminForm adminForm = new AdminForm();
                    adminForm.Show();
                    this.Close();
                    break;
                }
                if (user.user_type == "Диспетчер")
                {
                    DispatcherForm dispatcherForm = new DispatcherForm();
                    dispatcherForm.Show();
                    this.Close();
                    break;
                }
                if (user.user_type == "Робітник")
                {
                    WorkerForm workerForm = new WorkerForm();
                    workerForm.Show();
                    this.Close();
                    break;
                }
            }
            if(!successful)
            {
                MessageBox.Show("Введено невірні дані для входу.\nСпробуйте ще раз", "Помилка входу");
            }
        }
    }
}
