﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;
using CourseWork_CarService.Model;

namespace CourseWork_CarService.Views.Select
{
    /// <summary>
    /// Логика взаимодействия для SelectOrderByUser.xaml
    /// </summary>
    public partial class SelectOrderByUser : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public SelectOrderByUser(int userid, string user)
        {
            InitializeComponent();
            UserLabel.Content += user;
            getAllOrders(userid);
        }

        void getAllOrders(int clientid)
        {
            var result = from order in contextEntities.Orders
                join car in contextEntities.Car on order.Car_ID equals car.Car_ID
                join client in contextEntities.Client on car.Client_ID equals client.Client_ID
                where client.Client_ID == clientid
                select new OrderInformation()
                {
                    Order_Number = order.Order_ID,
                    Car_Number = car.CarNumber,
                    Client_Firstname = client.FirstName,
                    Client_Lastname = client.LastName,
                    Description = order.OrderDescription,
                    Price = order.OrderPrice,
                    Order_Discount = order.Discount,
                    Final_Price = order.PriceWithDiscount,
                    Date_Of_Order = order.DateOfOrder,
                    Implementation_Date = order.DateOfImplementation,
                    Status_Of_order = order.OrderStatus,
                    Status_Of_Payment = order.PaymentStatus
                };
            ClientOrdersGrid.ItemsSource = result.ToList();
        }
    }
}
