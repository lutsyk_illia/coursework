﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork_CarService.CarService_DB;

namespace CourseWork_CarService.Views
{
    /// <summary>
    /// Логика взаимодействия для WorkerForm.xaml
    /// </summary>
    public partial class WorkerForm : Window
    {
        CarService_CourseWorkEntities contextEntities = new CarService_CourseWorkEntities();
        public WorkerForm()
        {
            InitializeComponent();
            GetAllServices();
            GetAllServiceOrders();
        }

        void GetAllServiceOrders()
        {
            try
            {
                var result = from serviceOrder in contextEntities.ServiceOrder
                             join service in contextEntities.Service on serviceOrder.Service_ID equals service.Service_ID
                             join material in contextEntities.Material on serviceOrder.Material_ID equals material.Material_ID
                             join worker in contextEntities.Worker on serviceOrder.Worker_ID equals worker.Worker_ID
                             join position in contextEntities.Position on worker.Position_ID equals position.Position_ID
                             select new
                             {
                                 Order_Number = serviceOrder.Order_ID,
                                 Service = service.ServiceName,
                                 Material = material.MaterialName,
                                 Quantity = serviceOrder.NumberOfMaterial,
                                 Worker_Name = worker.FirsName,
                                 Worker_SurName = worker.LastName,
                                 Worker_Position = position.PositionName
                             };
                ChangeMaterials.ItemsSource = result.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        void GetAllServices()
        {
            try
            {
                var services = from service in contextEntities.Service
                               select new { Name = service.ServiceName, Price = service.ServicePrice, Execution_Time = service.NormalAmountOfTime };
                ServicesDataGrid.ItemsSource = services.ToList();
                var serviceNames = from service in contextEntities.Service
                                   select service.ServiceName;
                Services.ItemsSource = serviceNames.ToList();
                var materialNames = from material in contextEntities.Material
                                    select material.MaterialName;
                Materials.ItemsSource = materialNames.ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }

        private void FindOrder_Click(object sender, RoutedEventArgs e)
        {
            if (WorkerSurName.Text != "" && WorkerName.Text != "" && Positions.Text != "")
            {
                try
                {
                    var result = from serviceOrder in contextEntities.ServiceOrder
                                 join service in contextEntities.Service on serviceOrder.Service_ID equals service.Service_ID
                                 join material in contextEntities.Material on serviceOrder.Material_ID equals material.Material_ID
                                 join worker in contextEntities.Worker on serviceOrder.Worker_ID equals worker.Worker_ID
                                 join position in contextEntities.Position on worker.Position_ID equals position.Position_ID
                                 where worker.FirsName == WorkerName.Text && worker.LastName == WorkerSurName.Text &&
                                       position.PositionName == Positions.Text
                                 select new
                                 {
                                     Order_Number = serviceOrder.Order_ID,
                                     Service = service.ServiceName,
                                     Material = material.MaterialName,
                                     Quantity = serviceOrder.NumberOfMaterial,
                                     WorkerName = worker.FirsName,
                                     WorkerSurName = worker.LastName,
                                     Position = position.PositionName
                                 };
                    OrdersByWorker.ItemsSource = result.ToList();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Помилка");
            }
        }

        private void SetMaterial_Click(object sender, RoutedEventArgs e)
        {
            if (OrderNumber.Text != "" && Services.SelectedIndex != -1 && Materials.SelectedIndex != -1 &&
                MaterialQuantity.Text != "")
            {
                try
                {
                    var serviceID = (from services in contextEntities.Service
                        where services.ServiceName == Services.Text
                        select services.Service_ID).SingleOrDefault();
                    contextEntities.SetMaterial(Materials.Text,
                        int.Parse(MaterialQuantity.Text), serviceID, int.Parse(OrderNumber.Text));
                    contextEntities.SaveChanges();
                    GetAllServiceOrders();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show(exception.Message, "Error");
                }
            }
            else
            {
                MessageBox.Show("Заповніть всі поля", "Помилка");
            }
        }
    }
}
