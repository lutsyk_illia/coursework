//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CourseWork_CarService.CarService_DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class vPayCheck
    {
        public int Order_ID { get; set; }
        public string CarProducer { get; set; }
        public string Brand { get; set; }
        public string CarNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string ServiceName { get; set; }
        public Nullable<decimal> ServicePrice { get; set; }
        public string MaterialName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<System.DateTime> DateOfImplementation { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> PriceWithDiscount { get; set; }
    }
}
